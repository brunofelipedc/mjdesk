package com.example.mjdesk;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;

public class AbrirChamadoActivity extends Activity implements OnClickListener{
	
	private static final String NAMESPACE = "http://tcc.webservices/";
	private static String URL = "http://192.168.1.2:8080/WSJdesk/AbrirChamado?wsdl";
	private static final String METHOD_NAME = "abreChamado";
	private static final String SOAP_ACTION = "http://tcc.webservices/abreChamado";
    
	EditText campoNome;
	EditText campoData;
	EditText campoHora;
	EditText campoTelefone;
	EditText campoCelular;
	EditText campoEndereco;
	EditText campoProblema;
	TextView lbConfirmacao;
	Button botaoCadastrar;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_abrir_chamado);
		
		campoNome = (EditText) findViewById(R.id.campoCliente);
		campoData = (EditText) findViewById(R.id.campoData);
		campoHora = (EditText) findViewById(R.id.campoHora);
		campoTelefone = (EditText) findViewById(R.id.campoTelefone);
		campoCelular = (EditText) findViewById(R.id.campoCelular);
		campoEndereco = (EditText) findViewById(R.id.campoEndereco);
		campoProblema = (EditText) findViewById(R.id.campoProblema);
		lbConfirmacao = (TextView) findViewById(R.id.lbConfirmacao);
		
		View botaoCadastro = findViewById(R.id.btCadastrar);           
		botaoCadastro.setOnClickListener(this);
		
	}
	
	public void onClick(View v){
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		PropertyInfo propInfo = new PropertyInfo();
		propInfo.name = "arg0";
		propInfo.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo2 = new PropertyInfo();
		propInfo2.name = "arg1";
		propInfo2.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo3 = new PropertyInfo();
		propInfo3.name = "arg2";
		propInfo3.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo4 = new PropertyInfo();
		propInfo4.name = "arg3";
		propInfo4.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo5 = new PropertyInfo();
		propInfo5.name = "arg4";
		propInfo5.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo6 = new PropertyInfo();
		propInfo6.name = "arg5";
		propInfo6.type = PropertyInfo.STRING_CLASS;

		PropertyInfo propInfo7 = new PropertyInfo();
		propInfo7.name = "arg6";
		propInfo7.type = PropertyInfo.STRING_CLASS;

		request.addProperty(propInfo, campoNome.getText().toString());
		request.addProperty(propInfo2, campoData.getText().toString());
		request.addProperty(propInfo3, campoHora.getText().toString());
		request.addProperty(propInfo4, campoTelefone.getText().toString());
		request.addProperty(propInfo5, campoCelular.getText().toString());
		request.addProperty(propInfo6, campoEndereco.getText().toString());
		request.addProperty(propInfo7, campoProblema.getText().toString());

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
					.getResponse();

			lbConfirmacao.setText(resultsRequestSOAP.toString());

		} catch (Exception e) {

		}
		      
		 }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
