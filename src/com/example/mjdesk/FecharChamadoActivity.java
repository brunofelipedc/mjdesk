package com.example.mjdesk;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;

public class FecharChamadoActivity extends Activity implements OnClickListener {

	private static final String NAMESPACE = "http://tcc.webservices/";
	private static String URL = "http://192.168.1.2:8080/WSJdesk/FecharChamado?wsdl";
	private static final String METHOD_NAME = "fechaChamado";
	private static final String SOAP_ACTION = "http://tcc.webservices/fechaChamado";

	EditText campoId;
	TextView lbConfirmacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fechar_chamado);

		campoId = (EditText) findViewById(R.id.campoId);
		lbConfirmacao = (TextView) findViewById(R.id.lbConfirmacao);

		View botaoFechar = findViewById(R.id.btFechar);
		botaoFechar.setOnClickListener(this);

	}

	public void onClick(View v) {

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		PropertyInfo propInfo = new PropertyInfo();
		propInfo.name = "arg0";
		propInfo.type = PropertyInfo.STRING_CLASS;

		request.addProperty(propInfo,
				Long.parseLong(campoId.getText().toString()));

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
					.getResponse();

			lbConfirmacao.setText(resultsRequestSOAP.toString());

		} catch (Exception e) {

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
