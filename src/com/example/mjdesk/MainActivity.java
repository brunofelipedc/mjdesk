package com.example.mjdesk;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	Button btAbrirChamado;
	Button btListarChamados;
	Button btFecharChamado;
	Button btEditarChamado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btAbrirChamado = (Button) findViewById(R.id.btAbrirChamado);
		final Intent intent = new Intent(this, AbrirChamadoActivity.class);
		btAbrirChamado.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(intent);
			}
		});

		btListarChamados = (Button) findViewById(R.id.btListarChamados);
		final Intent intent2 = new Intent(this, ListarChamadosActivity.class);
		btListarChamados.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(intent2);
			}
		});

		btFecharChamado = (Button) findViewById(R.id.btFecharChamado);
		final Intent intent3 = new Intent(this, FecharChamadoActivity.class);
		btFecharChamado.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(intent3);
			}
		});

		btEditarChamado = (Button) findViewById(R.id.btEditarChamado);
		final Intent intent4 = new Intent(this, EditarChamadoActivity.class);
		btEditarChamado.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(intent4);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
