package com.example.mjdesk;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.TextSize;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;

public class ListarChamadosActivity extends Activity {

	private static final String NAMESPACE = "http://tcc.webservices/";
	private static String URL = "http://192.168.1.2:8080/WSJdesk/ListarChamados?wsdl";
	private static final String METHOD_NAME = "listaChamados";
	private static final String SOAP_ACTION = "http://tcc.webservices/listaChamados";

	TableLayout tab;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listar_chamados);

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		PropertyInfo propInfo = new PropertyInfo();
		propInfo.name = "arg0";
		propInfo.type = PropertyInfo.STRING_CLASS;

		request.addProperty(propInfo, 1);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
					.getResponse();

			String info = new String();

			info = resultsRequestSOAP.toString();

			String[] t = new String[1];
			t = info.split("#");
			int tamanho = Integer.parseInt(t[0]);

			String[] dados = new String[(tamanho * 9) + 1];

			dados = info.split("#");

			String[][] tabela = new String[tamanho][9];

			int j = 1;
			for (int i = 0; i < tamanho; i++) {
				for (int k = 0; k < 9; k++) {
					tabela[i][k] = dados[j];
					j++;
				}

			}

			tab = (TableLayout) findViewById(R.id.tabela);

			TableRow linhainicial = new TableRow(this);
			TextView li = new TextView(this);
			li.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			li.setText("Abaixo est�o listados todos os chamados");
			linhainicial.addView(li);
			tab.addView(linhainicial);

			TableRow linha1 = new TableRow(this);
			TextView l1 = new TextView(this);
			l1.setText("---------------------------------------------------");
			linha1.addView(l1);
			tab.addView(linha1);

			for (int k = 0; k < tamanho; k++) {
				for (int i = 0; i < 9; i++) {

					TableRow tr = new TableRow(this);

					TextView tv0 = new TextView(this);
					tv0.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					if (i == 0) {
						tv0.setText("ID: " + tabela[k][i]);
					}
					if (i == 1) {
						tv0.setText("Cliente: " + tabela[k][i]);
					}
					if (i == 2) {
						tv0.setText("Data: " + tabela[k][i]);
					}
					if (i == 3) {
						tv0.setText("Hora: " + tabela[k][i]);
					}
					if (i == 4) {
						tv0.setText("Telefone 1: " + tabela[k][i]);
					}
					if (i == 5) {
						tv0.setText("Telefone 2: " + tabela[k][i]);
					}
					if (i == 6) {
						tv0.setText("Endere�o: " + tabela[k][i]);
					}
					if (i == 7) {
						tv0.setText("Descri��o: " + tabela[k][i]);
					}
					if (i == 8) {
						tv0.setText("Status: " + tabela[k][i]);
					}

					tr.addView(tv0);

					tab.addView(tr);
				}
				TableRow linha = new TableRow(this);

				TextView l = new TextView(this);
				l.setText("---------------------------------------------------");

				linha.addView(l);

				tab.addView(linha);
			}

		} catch (Exception e) {

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
