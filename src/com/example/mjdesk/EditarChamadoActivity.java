package com.example.mjdesk;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;

public class EditarChamadoActivity extends Activity implements OnClickListener {

	private static final String NAMESPACE = "http://tcc.webservices/";
	private static String URL = "http://192.168.1.2:8080/WSJdesk/MostrarChamado?wsdl";
	private static final String METHOD_NAME = "mostraChamado";
	private static final String SOAP_ACTION = "http://tcc.webservices/mostraChamado";

	private static final String NAMESPACE2 = "http://tcc.webservices/";
	private static String URL2 = "http://192.168.1.2:8080/WSJdesk/EditarChamado?wsdl";
	private static final String METHOD_NAME2 = "editaChamado";
	private static final String SOAP_ACTION2 = "http://tcc.webservices/editaChamado";

	EditText campoId;
	TextView lbConfirmacao;
	TableLayout tab;
	EditText[] et;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editar_chamado);

		campoId = (EditText) findViewById(R.id.campoId);
		lbConfirmacao = (TextView) findViewById(R.id.lbConfirmacao);

		View botaoEditar = findViewById(R.id.btBuscar);
		botaoEditar.setOnClickListener(this);

	}

	public void onClick(View v) {

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		PropertyInfo propInfo = new PropertyInfo();
		propInfo.name = "arg0";
		propInfo.type = PropertyInfo.STRING_CLASS;

		request.addProperty(propInfo,
				Long.parseLong(campoId.getText().toString()));

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
					.getResponse();

			String info = new String();

			info = resultsRequestSOAP.toString();

			String[] t = new String[1];
			t = info.split("#");
			int tamanho = Integer.parseInt(t[0]);

			String[] dados = new String[(tamanho * 9) + 1];

			dados = info.split("#");

			final String[] tabela = new String[9];

			int j = 1;
			for (int i = 0; i < 9; i++) {

				tabela[i] = dados[j];
				j++;

			}

			et = new EditText[9];
			tab = (TableLayout) findViewById(R.id.tabela);

			TableRow linhainicial = new TableRow(this);
			TextView li = new TextView(this);
			li.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			li.setText("Informa��es do chamado de ID: " + tabela[0]);
			linhainicial.addView(li);
			tab.addView(linhainicial);

			TableRow linha1 = new TableRow(this);
			TextView l1 = new TextView(this);
			l1.setText("---------------------------------------------------");
			linha1.addView(l1);
			tab.addView(linha1);

			for (int i = 0; i < 9; i++) {

				TableRow tr = new TableRow(this);

				TextView tv0 = new TextView(this);

				tv0.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
				if (i == 0) {
					tv0.setText("ID:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
					et[i].setFocusable(false);
				}
				if (i == 1) {
					tv0.setText("Cliente:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 2) {
					tv0.setText("Data:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 3) {
					tv0.setText("Hora:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 4) {
					tv0.setText("Telefone 1:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 5) {
					tv0.setText("Telefone 2:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 6) {
					tv0.setText("Endere�o:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 7) {
					tv0.setText("Descri��o:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}
				if (i == 8) {
					tv0.setText("Status:");
					et[i] = new EditText(this);
					et[i].setText(tabela[i]);
				}

				tr.addView(tv0);

				TableRow l2 = new TableRow(this);

				l2.addView(et[i]);

				tab.addView(tr);

				tab.addView(l2);
			}

			TableRow linhabt = new TableRow(this);
			Button btEditar = new Button(this);
			btEditar.setText("Editar Chamado");
			btEditar.setWidth(30);
			linhabt.addView(btEditar);
			tab.addView(linhabt);

			btEditar.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					SoapObject request2 = new SoapObject(NAMESPACE2,
							METHOD_NAME2);

					PropertyInfo propInfo1 = new PropertyInfo();
					propInfo1.name = "arg0";
					propInfo1.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo2 = new PropertyInfo();
					propInfo2.name = "arg1";
					propInfo2.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo3 = new PropertyInfo();
					propInfo3.name = "arg2";
					propInfo3.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo4 = new PropertyInfo();
					propInfo4.name = "arg3";
					propInfo4.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo5 = new PropertyInfo();
					propInfo5.name = "arg4";
					propInfo5.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo6 = new PropertyInfo();
					propInfo6.name = "arg5";
					propInfo6.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo7 = new PropertyInfo();
					propInfo7.name = "arg6";
					propInfo7.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo8 = new PropertyInfo();
					propInfo8.name = "arg7";
					propInfo8.type = PropertyInfo.STRING_CLASS;

					PropertyInfo propInfo9 = new PropertyInfo();
					propInfo9.name = "arg8";
					propInfo9.type = PropertyInfo.STRING_CLASS;

					Long id = Long.parseLong(tabela[0].toString());
					String cliente = et[1].getText().toString();
					String data = et[2].getText().toString();
					String hora = et[3].getText().toString();
					String telefone = et[4].getText().toString();
					String celular = et[5].getText().toString();
					String endereco = et[6].getText().toString();
					String problema = et[7].getText().toString();
					String status = et[8].getText().toString();

					request2.addProperty(propInfo1, id);
					request2.addProperty(propInfo2, cliente);
					request2.addProperty(propInfo3, data);
					request2.addProperty(propInfo4, hora);
					request2.addProperty(propInfo5, telefone);
					request2.addProperty(propInfo6, celular);
					request2.addProperty(propInfo7, endereco);
					request2.addProperty(propInfo8, problema);
					request2.addProperty(propInfo9, status);

					SoapSerializationEnvelope envelope2 = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);

					envelope2.setOutputSoapObject(request2);
					HttpTransportSE androidHttpTransport2 = new HttpTransportSE(
							URL2);

					try {
						androidHttpTransport2.call(SOAP_ACTION2, envelope2);

						SoapPrimitive resultsRequestSOAP2 = (SoapPrimitive) envelope2
								.getResponse();

						lbConfirmacao.setText(resultsRequestSOAP2.toString());

					} catch (Exception e) {

					}

				}
			});

		} catch (Exception e) {

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
